//
//  AppDelegate.h
//  Ecosia Task
//
//  Created by Granit Gjevukaj on 16/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


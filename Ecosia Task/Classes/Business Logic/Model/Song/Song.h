//
//  Song.h
//  Ecosia Task
//
//  Created by Granit Gjevukaj on 17/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Song : NSObject

@property(strong, nonatomic) NSString* songTitle;
@property(strong, nonatomic) NSString* songArtistName;
@property(strong, nonatomic) NSString* songAlbumName;

@property(strong, nonatomic) NSURL* songSourceURL;

@property(strong, nonatomic) UIImage* songAlbumCoverArt;

@end

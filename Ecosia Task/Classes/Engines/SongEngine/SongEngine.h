//
//  SongEngine.h
//  Ecosia Task
//
//  Created by Granit Gjevukaj on 17/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Song.h"

typedef void (^SongEngineCompletion)(BOOL success, Song* song);

@interface SongEngine : NSObject

-(void)shuffleSong:(SongEngineCompletion)completion;

+(id)sharedInstance;

@end

//
//  SongEngine.m
//  Ecosia Task
//
//  Created by Granit Gjevukaj on 17/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "SongEngine.h"

#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVMetadataItem.h>

@implementation SongEngine

#pragma mark - Shuffle Song

-(void)shuffleSong:(SongEngineCompletion)completion {
    
    int randomNumber = arc4random() % 4 + 1;
    
    NSURL *soundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"Song%d", randomNumber] ofType:@"mp3"]];
    
    AVURLAsset* asset = [AVURLAsset assetWithURL:soundURL];
    
    NSArray *titles = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyTitle keySpace:AVMetadataKeySpaceCommon];
    NSArray *artists = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyArtist keySpace:AVMetadataKeySpaceCommon];
    NSArray *albumNames = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata withKey:AVMetadataCommonKeyAlbumName keySpace:AVMetadataKeySpaceCommon];
    
    AVMetadataItem *title = [titles objectAtIndex:0];
    AVMetadataItem *artist = [artists objectAtIndex:0];
    AVMetadataItem *albumName = [albumNames objectAtIndex:0];
    
    Song* song = [Song new];
    song.songSourceURL = soundURL;
    song.songTitle = (NSString *)title.value;
    song.songArtistName = (NSString *)artist.value;
    song.songAlbumName = (NSString *)albumName.value;
    
    NSArray *keys = [NSArray arrayWithObjects:@"commonMetadata", nil];
    [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        NSArray *artworks = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata
                                                           withKey:AVMetadataCommonKeyArtwork
                                                          keySpace:AVMetadataKeySpaceCommon];
        UIImage *albumArtWork;
        
        for (AVMetadataItem *item in artworks) {
            if ([item.keySpace isEqualToString:AVMetadataKeySpaceID3]) {
                
                if (TARGET_OS_IPHONE && NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
                    NSData *newImage = [item.value copyWithZone:nil];
                    albumArtWork = [UIImage imageWithData:newImage];
                }
                else {
                    NSDictionary *dict = [item.value copyWithZone:nil];
                    if ([dict objectForKey:@"data"]) {
                        albumArtWork = [UIImage imageWithData:[dict objectForKey:@"data"]];
                    }
                }
            }
            else if ([item.keySpace isEqualToString:AVMetadataKeySpaceiTunes]) {
                albumArtWork = [UIImage imageWithData:[item.value copyWithZone:nil]];
            }
            
            song.songAlbumCoverArt = albumArtWork;
        }
    }];
    
    completion(YES, song);
}

#pragma mark - Singleton

+ (id)sharedInstance {
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedInstance = nil;
    dispatch_once(&pred, ^{
        _sharedInstance = [[self alloc] init];
        SongEngine* engine = (SongEngine *)_sharedInstance;
    });
    return _sharedInstance;
    
}

- (id)init {
    self = [super init];
    if(self) {
    }
    return self;
}

@end

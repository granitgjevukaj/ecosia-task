//
//  HomeViewController.m
//  Ecosia Task
//
//  Created by Granit Gjevukaj on 16/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import "HomeViewController.h"

#import <AVFoundation/AVFoundation.h>

#import "SongEngine.h"

@interface HomeViewController ()

// Navigation Bar
@property(strong, nonatomic) UIView* navBar;
@property(strong, nonatomic) UILabel* titleLabel;

// Album Cover
@property(strong, nonatomic) UIImageView* albumCoverImageView;

// Meta Data
@property(strong, nonatomic) UILabel* songTitleLabel;
@property(strong, nonatomic) UILabel* songArtistLabel;
@property(strong, nonatomic) UILabel* songAlbumLabel;


// Shuffle Song Button
@property(strong, nonatomic) UIButton* shuffleSongButton;

// Play Butotn
@property(strong, nonatomic) UIButton* playButton;

@property(strong, nonatomic) AVAudioPlayer *soundPlayer;

@property BOOL isPlaying;

@end

@implementation HomeViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Setup Views
    [self setupNavigationBar];
    [self setupViews];
}

#pragma mark - Setup Navigation Bar

-(void)setupNavigationBar {
    
    self.navBar = [[UIView alloc] init];
    self.navBar.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:62.0/255.0 alpha:1.0];
    [self.view addSubview:self.navBar];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"Random Music Player";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    [self.navBar addSubview:self.titleLabel];
    
    self.playButton = [[UIButton alloc] init];
    self.playButton.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:62.0/255.0 alpha:1.0];
    [self.playButton.imageView setContentMode:UIViewContentModeCenter];
    [self.playButton.titleLabel setTextColor:[UIColor whiteColor]];
    [self.playButton addTarget:self action:@selector(pauseSongAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.playButton setHidden:YES];
    [self.view addSubview:self.playButton];
}

#pragma mark - Setup Views

-(void)setupViews {
    
    // Cover Art
    self.albumCoverImageView = [[UIImageView alloc] init];
    self.albumCoverImageView.image = [UIImage imageNamed:@"ic_placeholder"];
    self.albumCoverImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.albumCoverImageView];
    
    // Meta Data
    self.songTitleLabel = [[UILabel alloc] init];
    self.songTitleLabel.text = @"Title";
    self.songTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.songTitleLabel];
    
    self.songArtistLabel = [[UILabel alloc] init];
    self.songArtistLabel.text = @"Artist";
    self.songArtistLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.songArtistLabel];
    
    self.songAlbumLabel = [[UILabel alloc] init];
    self.songAlbumLabel.text = @"Album";
    self.songAlbumLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.songAlbumLabel];
    
    // Shuffle Song Button
    self.shuffleSongButton = [[UIButton alloc] init];
    self.shuffleSongButton.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:55.0/255.0 blue:62.0/255.0 alpha:1.0];
    [self.shuffleSongButton setTitle:@"Shuffle Song" forState:UIControlStateNormal];
    [self.shuffleSongButton.titleLabel setTextColor:[UIColor whiteColor]];
    [self.shuffleSongButton addTarget:self action:@selector(shuffleSongAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.shuffleSongButton];
}

#pragma mark - Layout

- (void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    // Navigation Bar
    {
        self.navBar.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 100.0);
        self.titleLabel.frame = CGRectMake(0, 50, CGRectGetWidth(self.view.bounds), 50.0);
        self.playButton.frame = CGRectMake(CGRectGetWidth(self.view.bounds) - 70.0, 50, 44.0, 44.0);
    }
    
    // Cover Art
    self.albumCoverImageView.frame = CGRectMake(0, CGRectGetMaxY(self.navBar.frame), CGRectGetWidth(self.view.bounds), CGRectGetWidth(self.view.bounds));
    
    // Meta Data
    self.songTitleLabel.frame = CGRectMake(30.0, CGRectGetMaxY(self.albumCoverImageView.frame) + 10.0, CGRectGetWidth(self.view.bounds) - 60.0, 20.0);
    self.songArtistLabel.frame = CGRectMake(30.0, CGRectGetMaxY(self.songTitleLabel.frame) + 10.0, CGRectGetWidth(self.view.bounds) - 60.0, 20.0);
    self.songAlbumLabel.frame = CGRectMake(30.0, CGRectGetMaxY(self.songArtistLabel.frame) + 10.0, CGRectGetWidth(self.view.bounds) - 60.0, 20.0);

    // Shuffle Song Button
    self.shuffleSongButton.frame = CGRectMake(30.0, CGRectGetHeight(self.view.bounds) - 75.0, CGRectGetWidth(self.view.bounds) - 60.0, 50.0);
    
}

#pragma mark - Shuffle Action

-(void)shuffleSongAction:(UIButton *)sender {
    
    NSTimer * myTimer = nil;
    
    [[SongEngine sharedInstance] shuffleSong:^(BOOL success, Song *song) {
        if (success) {
            self.soundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:song.songSourceURL error:nil];
            
            [self.soundPlayer prepareToPlay];
            [self.soundPlayer play];
            
            [self.albumCoverImageView setImage:song.songAlbumCoverArt];
            [self.songTitleLabel setText:song.songTitle];
            [self.songArtistLabel setText:song.songArtistName];
            [self.songAlbumLabel setText:song.songAlbumName];
            
        }
    }];
    
    myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimeLeft) userInfo:nil repeats:YES];
    
    self.isPlaying = YES;
    [self.playButton setHidden:NO];
    [self.playButton setImage:[UIImage imageNamed:@"ic_pause"] forState:UIControlStateNormal];
}

#pragma mark - Pause Action

-(void)pauseSongAction:(UIButton *)sender {
    
    if (self.isPlaying) {
        [self.soundPlayer pause];
        [self.playButton setImage:[UIImage imageNamed:@"ic_play"] forState:UIControlStateNormal];
        
        self.isPlaying = NO;
    } else {
        
        [self.soundPlayer play];
        [self.playButton setImage:[UIImage imageNamed:@"ic_pause"] forState:UIControlStateNormal];
        
        self.isPlaying = YES;
    }
}

- (void)updateTimeLeft {
    
    NSTimeInterval timeLeft = self.soundPlayer.duration - self.soundPlayer.currentTime;
    [self.titleLabel setText:[self stringFromTimeInterval:timeLeft]];
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld",(long)minutes, (long)seconds];
}

@end

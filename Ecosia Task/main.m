//
//  main.m
//  Ecosia Task
//
//  Created by Granit Gjevukaj on 16/03/2017.
//  Copyright © 2017 Granit Gjevukaj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
